from flask import Flask, request
from flask_mongoengine import MongoEngine
from flask_mail import Mail
from flask_admin import Admin
from flask_admin.contrib.mongoengine import ModelView
# from flask_wtf.csrf import CSRFProtect

from web.mail_service import dbhelper
from flask_debugtoolbar import DebugToolbarExtension

db = MongoEngine()
mail = Mail()
admin = Admin()
# csrf = CSRFProtect()

app = Flask(__name__)
app.debug = True

app.config.from_object('web.settings')
app.config['SECRET_KEY'] = '<replace with a secret key>'
app.config['WTF_CSRF_SECRET_KEY'] = 'a secret key'
app.secret_key = 's3cr3t'

toolbar = DebugToolbarExtension(app)
app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False
db.init_app(app)
mail.init_app(app)
admin.init_app(app)
# csrf.init_app(app)
from web.mail_service import admin

@app.before_request
def connect_mail():
    request.mail = mail


@app.before_request
def connect_test():
    request.test = 'test'


from web.mail_service.views import mail
app.register_blueprint(mail.module, url_prefix='/mail')


