import datetime

from flask_mongoengine.wtf import model_form
from wtforms import Form, BooleanField, StringField, PasswordField, validators, fields

from web.mail_service.models import Event, gen_uid, Ticket

EventForm = model_form(Event, exclude={'created_at', 'last_modified'})
TicketForm = model_form(Ticket, exclude={'id', 'created_at', 'last_modified'})


class SendMailForm(Form):
    event_list = Event.objects.all()
    event_list = list((e.name, e.name) for e in event_list)
    event = fields.SelectField('Event', [validators.data_required()], choices=event_list)
    csv_file = fields.FileField()


