# Admin page
from flask_admin.contrib.mongoengine import ModelView

from web import admin
from web.mail_service.models import Event, Ticket, QRCode, TikkitUser, Company, Order

admin.add_view(ModelView(Event))
admin.add_view(ModelView(Ticket))
admin.add_view(ModelView(QRCode))
admin.add_view(ModelView(TikkitUser))
admin.add_view(ModelView(Company))
admin.add_view(ModelView(Order))