import random
import time

import os
import paramiko
import qrcode
from datetime import datetime

from mongoengine import CASCADE

from web import db


def gen_uid():
    """Generate unique id based on timestamp"""
    strs = 'ABCDEFGHJKLMNPQRSTUVWXYZ'
    char = random.choice(strs)
    char2 = random.choice(strs)
    rand = int(round(time.time(), 3) * 1)
    return "{}{}{}".format(rand, char, char2)


class Event(db.Document):
    MUSIC = "Music Event"
    CONFERENCE = "Conference"

    EVENT_CATEGORY = (
        (MUSIC, 'Music Event'),
        (CONFERENCE, 'Conference'),
    )
    name = db.StringField(max_length=200, unique=True)
    start_date = db.DateTimeField(null=True, blank=True, default=datetime.now())
    end_date = db.DateTimeField(null=True, blank=True)
    tel = db.IntField(max_length=20, null=True, blank=True)
    city = db.StringField(max_length=255, null=True, blank=True)
    address = db.StringField(max_length=255, null=True, blank=True)
    website = db.StringField(max_length=255, null=True, blank=True)
    category = db.StringField(default=MUSIC, choices=EVENT_CATEGORY, max_length=20)
    created_at = db.DateTimeField(default=datetime.today(), format='%d/%m/%y - %H:%M')
    last_modified = db.DateTimeField(default=datetime.today(), format='%d/%m/%y - %H:%M')

    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = datetime.today()
        self.last_modified = datetime.today()
        return super(Event, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    # def __init__(self, event_name, event_address, event_date):
    #     print (type(event_date))
    #     self.name = event_name
    #     self.address = event_address
    #     self.start_date = event_date


class Ticket(db.Document):
    event = db.ReferenceField(Event, related_name="event", null=True)
    id = db.StringField(max_length=32, primary_key=True, default=gen_uid)
    name = db.StringField(max_length=200)
    price = db.FloatField(default=0)
    quantity = db.IntField(default=0)
    created_at = db.DateTimeField(default=datetime.today(), format='%d/%m/%y - %H:%M')
    last_modified = db.DateTimeField(default=datetime.today(), format='%d/%m/%y - %H:%M')

    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = datetime.today()
        self.last_modified = datetime.today()
        return super(Ticket, self).save(*args, **kwargs)

#   tag =

    def __str__(self):
        return self.name

    def get_event(self):
        return self.event

    def decrease_quantity(self):
        self.quantity -= 1
        self.save()


class Company(db.Document):
    name = db.StringField(primary_key=True)
    address = db.StringField(null=True, blank=True)
    tel = db.IntField(null=True, blank=True)
    fax = db.IntField(null=True, blank=True)

    def __str__(self):
        return self.name


class TikkitUser(db.Document):
    MAN = 'man'
    WOMAN = 'woman'

    GENDER_CHOICES = (
        (MAN, 'Male'),
        (WOMAN, 'Female'),
    )
    id = db.StringField(help_text='Customer id used by Tikkit system',
                           max_length=32, default=gen_uid, primary_key=True)
    first_name = db.StringField(max_length=50, blank=True, null=True)
    last_name = db.StringField(max_length=50, blank=True, null=True)
    gender = db.StringField(max_length=20, choices=GENDER_CHOICES, blank=True)
    phone = db.StringField(max_length=20, unique=False, null=True, blank=True)
    city = db.StringField(max_length=255, null=True, blank=True)
    address = db.StringField(max_length=255, null=True, blank=True)
    about = db.StringField(max_length=1000, null=True, blank=True)
    birth_day = db.DateTimeField(null=True, blank=True, format='%Y-%m-%d')
    # avatar = ImageField(upload_to=get_avatar_name, null=True, storage=OverwriteStorage(), blank=True)
    email = db.EmailField(unique=True, blank=True, null=True)
    company = db.ReferenceField(Company, blank=True, null=True)
    job = db.StringField(blank=True, null=True)
    created_at = db.DateTimeField(default=datetime.today(), format='%d/%m/%y - %H:%M')
    last_modified = db.DateTimeField(default=datetime.today(), format='%d/%m/%y - %H:%M')

    def __str__(self):
        return str(self.last_name) + str(self.first_name)

    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = datetime.today()
        self.last_modified = datetime.today()
        return super(TikkitUser, self).save(*args, **kwargs)

# class TikkitCustomer(db.Document):
#     tikkit_user = OneToOneField(TikkitUser,primary_key=True, related_name='tikkit_customer')
#     id = db.StringField(help_text='Customer id used by Tikkit system',
#                           max_length=32, unique=True)
#
#
# class TikkitAccount(db.Document):
#     INDIVIDUAL = 'individual'
#


class QRCode(db.Document):
    id = db.StringField(max_length=100, primary_key=True)
    src = db.StringField(max_length=254, unique=True, null=True)
    check = db.BooleanField(default=False)
    # order = db.ReferenceField(Order, null=True)
    customer = db.ReferenceField(TikkitUser, null=True)
    ticket = db.ReferenceField(Ticket, null=True)
    created_at = db.DateTimeField(default=datetime.today(), format='%d/%m/%y - %H:%M')
    last_modified = db.DateTimeField(default=datetime.today(), format='%d/%m/%y - %H:%M')

    def __str__(self):
        return str(self.id) + ' - '+str(self.ticket.name)

    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = datetime.today()
        self.last_modified = datetime.today()
        return super(QRCode, self).save(*args, **kwargs)

    def gen_qrcode(self, barcode_string, event_name=''):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=4,
        )
        qr.add_data(barcode_string)
        qr.make(fit=True)
        #save image to local
        img = qr.make_image()
        local_path = r'web/mail_service/static/barcode-images/{}'.format(event_name)
        remote_path = r'/var/www/tikkit/public_html/wp-content/uploads/barcodes/{}'.format(event_name)
        try:
            img.save(r'{}/{}.png'.format(local_path, barcode_string))
        except FileNotFoundError:
            os.mkdir(local_path)
            img.save(r'{}/{}.png'.format(local_path, barcode_string))
    # SSH Connection
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect('tikkit.vn', username='root', password='tikkit.vn@')
        ftp = ssh.open_sftp()
        try:
            ftp.chdir(remote_path)  # Test if remote_path exists
        except IOError:
            ftp.mkdir(remote_path)  # Create remote_path
            ftp.chdir(remote_path)
        remote_file_path = r'{}/{}.png'.format(remote_path, barcode_string)
        local_file_path = r'{}/{}.png'.format(local_path,barcode_string)
        ftp.put(local_file_path, remote_file_path)
        ssh.close()

        # code = QRCode.objects.get(pk=barcode_string)
        # update database
        url_file = r'http://tikkit.vn/wp-content/uploads/barcodes/{}/{}.png'.format(event_name, barcode_string)
        self.src = url_file
        self.ticket.decrease_quantity()
        self.id = barcode_string


def gen_orderid():
    """Generate unique id based on timestamp"""
    number_orders = QRCode.objects.all().count()
    id = number_orders + 3000
    return "{}".format(id)


class Order(db.Document):
    ORDER_CHARGE_SUCCEEDED = 'succeeded'
    ORDER_CHARGE_PENDING = 'pending'
    ORDER_CHARGE_FAILED = 'failed'

    INCOMPLETE = 'incomplete'  # Reservation is not completed yet, it's being filled
    WAITING = 'waiting'  # Waiting for validation of traveller
    ACCEPTED = 'accepted'  # Traveller accept the reservation
    REFUSED = 'refused'  # Traveller refuse the reservation
    DELIVERED = 'delivered'  # Traveller delivered the package

    STATUS_CHOICES = (
        (INCOMPLETE, 'incomplete'),
        (WAITING, 'waiting'),
        (ACCEPTED, 'accepted'),
        (REFUSED, 'refused'),
        (DELIVERED, 'delivered'),
    )
    order_id = db.SequenceField(primary_key=True)
    customer = db.ReferenceField(TikkitUser, related_name="tikkit_customer", null=True)
    qrcodes = db.ListField(db.ReferenceField(QRCode, related_name="qrcodes", null=True, reverse_delete_rule=CASCADE))                                        # ----------------------------?-----------------------
    order_date = db.DateTimeField(null=True, blank=True, default=None, format='%d/%m/%y - %H:%M')
    ship_address = db.StringField(null=True, blank=True)
    note = db.StringField(null=True, max_length=200, blank=True)
    last_modified = db.DateTimeField(format='%d/%m/%y - %H:%M')

    def __str__(self):
        return r'#{} - {}: {}'.format(self.id, self.customer.name, self.tickets)

    def save(self, *args, **kwargs):
        self.last_modified = datetime.today()
        return super(Order, self).save(*args, **kwargs)

    # def create_order(self,ticket_id):