from flask import Flask, Blueprint, render_template, abort, redirect, url_for, request, current_app
from jinja2 import TemplateNotFound
from mongoengine import DoesNotExist
from flask_mongoengine.wtf import model_form

# from web import csrf
from wtforms import widgets

from web.mail_service import tools
from web.mail_service.forms import EventForm, TicketForm, SendMailForm
from web.mail_service.models import Event,Ticket

# tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../templates/home')
# static_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../static')

module = Blueprint('mail', __name__, template_folder='../templates', static_folder='../static')


@module.route("/", methods=['GET', 'POST'])
@module.route("/<query_event>")
def home(query_event=None):
    list_ticket = []
    event_list = Event.objects.all()
    try:
        if query_event is not None:
            event = Event.objects.get(name=query_event)
            list_ticket = Ticket.objects.filter(event=event)
        return render_template('sites/home.html', event_list=event_list, list_ticket=list_ticket)
    except DoesNotExist:
        print("Can't find event")
        abort(404)
    except TemplateNotFound:
        print("Template not found")
        abort(404)


@module.route('/event', methods=['GET', 'POST'])
# @csrf.exempt
def event():
    # event = Event.objects.all()
    form = EventForm(request.form)
    if request.method == 'POST' and form.validate():
        data = form.data
        del data['csrf_token']
        Event(**data).save()
        return redirect(url_for('mail.home'))
    return render_template('sites/event.html', form=form)


@module.route('/ticket', methods=['GET', 'POST'])
def ticket():
    form = TicketForm(request.form)
    if request.method == 'POST' and form.validate():
        data = form.data
        del data['csrf_token']
        Ticket(**data).save()
        return redirect(url_for('mail.home'))
    return render_template('sites/ticket.html', form=form)


@module.route("/send-mail", methods=['GET', 'POST'])
def send_mail():
    form = TicketForm(request.form)
    if request.method == 'POST':
        event_id = request.form['event']
        event = Event.objects.get(pk=event_id)
        csv_file = request.files['csv_file']
        if tools.check_csv_file(csv_file):
            list_user = tools.read_csv_file(csv_file)
            summary = tools.send_mail(list_user, event)
            from pprint import pprint
            pprint(summary)
            return render_template('sites/upload_file.html', form=form)
        else:
            print("Failed to open File!")
    return render_template('sites/upload_file.html', form=form)


@module.route("/home")
def index():
    return render_template('sites/base.html')