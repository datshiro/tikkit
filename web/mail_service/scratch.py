from web.mail_service.models import Event
from mongoengine import connect
from web.mail_service import dbhelper
import json

# mongo = dbhelper.DBHelper()

# mongo = connect('Mail_Service')

event = Event(name="Traproom XII")
event.category = Event.MUSIC
event.address = "Envy Club"
event.start_date = "2018-01-11"
event.end_date = "2018-01-11"
event.website = "http://tikkit.vn/traproomxii/"
parsed = json.loads(event.to_json())
print(json.dumps(parsed, indent=4))

event.save()

# mongo.db.close()
