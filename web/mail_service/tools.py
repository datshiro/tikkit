# -*- coding: utf-8 -*-
import random
import time
from smtplib import SMTPSenderRefused, SMTPDataError

from flask import render_template, jsonify, json
from mongoengine import DoesNotExist
from werkzeug.utils import secure_filename

from web import mail
from web.mail_service.models import Ticket, QRCode, TikkitUser, Company
from flask_mail import Message
import shlex

def gen_random_string(prefix=None, length=None, suffix=None):
    """Generate a random string based on timestamp"""

    strs = 'ABCDEFGHJKLMNPQRSTUVWXYZ'
    if prefix is None:
        char = random.choice(strs)
        char2 = random.choice(strs)
        prefix = char + char2
    if suffix is None:
        suffix = random.choice(strs)
    if length is None:
        length = 7
    rand = int(round(time.time(), 3) % eval('1' + str(0) * length))
    return "{}{}{}".format(prefix, rand, suffix)


def check_csv_file(csv_file):
    """
    Check if uploaded file is csv file type
    :param csv_file: FileStorage
    :return: True if csv file
    """
    file_name = secure_filename(csv_file.filename)
    if not file_name.endswith('.csv'):
        print("File is not CSV type")
        return False
    # if file is too large, return
    # if csv_file.multiple_chunks():
    #     print("Uploaded file is too big (%.2f MB)." % (csv_file.size / (1000 * 1000),))
    #     return None
    return True


def read_csv_file(csv_file):
    """
    Read CSV File and return a list of user-dictionary
    :param csv_file: <class 'werkzeug.datastructures.FileStorage'>
    :return:
    """
    file_data = csv_file.read().decode("utf-8")
    lines = file_data.split("\n")
    list_record = []

    title = lines.pop(0)    # Get title - which is the first line
    print("Title -", title)
    title = [field.lower().replace(' ', '_') for field in title.split("|")]     # lowercase title

    # loop over the lines and save them in db. If error , store as string and then display
    for line in lines:
        if len(line) <= 1:  # ignore empty lines
            continue
        fields = line.split("|")
        data_dict = {}
        for i in range(0, len(fields)):
            # if fields[i] in '\r':
            #     fields[i].replace("\r", " ")
            data_dict[title[i]] = fields[i]
        list_record.append(data_dict)
    return list_record


def compose_mail(user, event, subject=None):
    ticket = Ticket.objects.get(event=event, name=str(user["ticket"]).upper())
    data = user
    qrcode = gen_qrcode(ticket_id=ticket.id)
    qrcode.customer = get_user(**user)
    data['qrcode'] = qrcode.save()
    data['ticket'] = ticket
    data['name'] = '{}, {}'.format(user["first_name"], user["last_name"])
    # SHOP STORE VIETNAM
    # data['ssv03'] = 1
    # data['ssv02'] = 0
    # data['ssv01'] = 0
    # if data['ticket'] == "COMBO 2 - DAY VIETNAM RETAIL & FRANCHISE CONFERENCE (March 28, 29 2018)":
    #     data['ssv01'] = 1
    #     data['ssv02'] = 1
    # elif data['ticket'] == "#2 VIETNAM RETAIL & FRANCHISE CONFERENCE ( DAY 2 - Thursday, March 29)":
    #     data['ssv02'] = 1
    # elif data['ticket'] == "#1 VIETNAM RETAIL & FRANCHISE CONFERENCE ( DAY 1 - Wedesday, March 28)":
    #     data['ssv01'] = 1
    # SHOP STORE VIETNAME ENDING
    eticket_html = 'emails/{}-eticket.html'.format(event.name.lower().replace(' ', ''))
    msg = render_template(eticket_html, data=data)

    if subject is None:
        subject = '[TikkiT Viet Nam] ' + event.name + ' - Eticket'

    message = Message(
        subject=subject,
        html=msg,
        sender=('TikkiT Việt Nam', 'info@tikkit.vn'),
        recipients=[data['email']],
        # bcc=[STRIKE_ADMIN_BCC],
        # reply_to=['nhat.minh@tikkit.vn'],
    )
    return message



def gen_random_qrcode(ticket_id, prefix=None, len=None, suffix=None):
    """
        generate a random qr code of a ticket
        this function does not save generated-qrcode to database
    """
    ticket = Ticket.objects.get(pk=ticket_id)

    random_string = gen_random_string(prefix, len, suffix)
    while QRCode.objects.filter(pk=random_string):
        random_string = gen_random_string(prefix, len, suffix)
    random_qrcode = QRCode(ticket=ticket).gen_qrcode(random_string)
    # print("===========> Random QRCode ", random_qrcode.id)
    return random_qrcode


def gen_qrcode(ticket_id, code_string=None, prefix=None, length=None, suffix=None):
    """
        generate a random qr code of a ticket
        this function does not save generated-qrcode to database
    """
    ticket = Ticket.objects.get(pk=ticket_id)
    if code_string is None:
        code_string = gen_random_string(prefix, length, suffix)
        while QRCode.objects.filter(pk=code_string):
            code_string = gen_random_string(prefix, length, suffix)
    else:
        if QRCode.objects.filter(pk=code_string):
            raise Exception("Can't not create this qrcode")
            return None
    qr_code = QRCode(ticket=ticket)
    qr_code.gen_qrcode(code_string, event_name=str(ticket.event.name).replace(' ', ''))

    print("===========> Random QRCode ", qr_code.id)
    return qr_code


def send_mail(list_user, event):
    sent_mail_list = []
    failed_mail_list = []
    with mail.connect() as conn:
        for user in list_user:
            print(user)
            create_user(user)
            quantity = user['quantity']
            for x in range(0, int(quantity)):
                message = '...'
                print('#' + str(x+1), user)
                # print('#' + str(x + 1), user["first_name"] + user["last_name"], user["email"], user["phone"], user["ticket"], user["quantity"], event)
                subject = '[TikkiT Viet Nam] {} - Eticket # {}'.format(event.name, str(x+1))
                message = compose_mail(user, event, subject)
                try:
                    conn.send(message)
                    sent_mail_list.append(user['email'])
                except SMTPSenderRefused or SMTPDataError:
                    failed_mail_list.append(user['email'])

    output = {'Succedded': sent_mail_list,
              'Failed': failed_mail_list}
    return output


def create_user(data):
    if not get_user(**data):
#        del data['id']                    # use default gen_uid instead from input
        fields = [k for k in TikkitUser._fields.keys() if k in data]
        user_info = {}
        for field in fields:
            user_info[field] = data[field]
        print(user_info)
        TikkitUser(**user_info).save()
        try:
            if not Company.objects.filter(name=user_info['company']) and user_info.__contains__('company'):
                Company(name=user_info['company']).save()
        except KeyError:
            pass
        print("User Created !!!")
        return True
    return False


def get_user(id=None, first_name=None, last_name=None, birth_day=None, email=None, *args, **kwargs):
    try:
        if id is not None:
            return TikkitUser.objects.get(id=id)
        else:
            return TikkitUser.objects.get(birth_day=birth_day, email=email)
    except DoesNotExist:
        return False


if __name__ == '__main__':
    csv_file = open(r'test-file.csv','r')
    print(csv_file.read())
    list_user = read_csv_file(csv_file)
    for user in list_user:
        create_user(user)

