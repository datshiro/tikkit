import mongoengine
import web.settings


DATABASE = "Mail_Service"


class DBHelper:
    def __init__(self):
        self.db = mongoengine.connect(DATABASE)
