from web import app

if __name__ == '__main__':
    app.run(port=1234, host='127.0.0.1', debug=True)
